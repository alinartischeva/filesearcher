﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;
using System.Runtime.CompilerServices;


namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {        
        static ManualResetEvent manualResetEvent = new ManualResetEvent(true);
        bool mre = false;
        List<string> paths = new List<string>();
        Stopwatch sw = new Stopwatch();   
        CancellationTokenSource ts = new CancellationTokenSource();
        CancellationToken ct = new CancellationToken();
        long countn = 0;
        public Form1()
        {
            InitializeComponent();
            directory.Text = Properties.Settings.Default.directory;
            filemask.Text = Properties.Settings.Default.filemask;
            filecontains.Text = Properties.Settings.Default.filecontains;
        }        
                       
        void DirSearch(string sDir, CancellationToken ct) //поиск файлов в папке 
        {   
            if (ct.IsCancellationRequested)
            {
                return;
            }
            Regex regex = new Regex(filemask.Text, RegexOptions.IgnoreCase);
            try            
            {
                foreach (string file in Directory.GetFiles(sDir, "*", SearchOption.TopDirectoryOnly))
                {
                    manualResetEvent.WaitOne();
                    countn++;
                    count.Invoke((MethodInvoker)(() => count.Text = countn.ToString()));
                    currentfiletextbox.Invoke((MethodInvoker)(() => currentfiletextbox.Text = file));                                        
                    if (regex.IsMatch(Path.GetFileName(file)) == true) //сравнение по маске имени
                    {
                        if (File.ReadLines(file).Any(line => line.Contains(filecontains.Text)) == true) //поиск содержимого в файле
                        {
                            Console.WriteLine(file);
                            paths.Add(file);                             
                            treeView1.Invoke((MethodInvoker)(() => PopulateTreeView(treeView1, paths, '\\')));
                        }
                    }
                    if (ct.IsCancellationRequested) {
                        return;
                    }
                }
                foreach (string d in Directory.GetDirectories(sDir))
                {
                    DirSearch(d, ct);
                }

            }
            catch (System.Exception e)
            {
                MessageBox.Show(e.ToString(), "Ошибка",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }
        private static void PopulateTreeView(TreeView treeView, IEnumerable<string> paths, char pathSeparator)
        {
            TreeNode lastNode = null;
            string subPathAgg;
            foreach (string path in paths)
            {
                subPathAgg = string.Empty;
                foreach (string subPath in path.Split(pathSeparator))
                {
                    subPathAgg += subPath + pathSeparator;
                    TreeNode[] nodes = treeView.Nodes.Find(subPathAgg, true);
                    if (nodes.Length == 0)
                        if (lastNode == null)
                            lastNode = treeView.Nodes.Add(subPathAgg, subPath);
                        else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath);
                    else
                        lastNode = nodes[0];
                }
            }
        }
        private void startbutton_Click(object sender, EventArgs e)
        {
            ct = ts.Token;
            countn = 0;
            sw.Reset();
            time.Text = "00:00:00";
            timer1.Start();
            sw.Start();
            manualResetEvent.Set();
            treeView1.Nodes.Clear();

            startbutton.Invoke((MethodInvoker)(() => startbutton.Enabled = false));
            pause.Invoke((MethodInvoker)(() => pause.Enabled = true));
            stop.Invoke((MethodInvoker)(() => stop.Enabled = true));
            directory.Invoke((MethodInvoker)(() => directory.Enabled = false));
            filemask.Invoke((MethodInvoker)(() => filemask.Enabled = false));
            filecontains.Invoke((MethodInvoker)(() => filecontains.Enabled = false));
         
            Task.Factory.StartNew(() =>
            {
                DirSearch(directory.Text, ct);
                timer1.Stop();
                MessageBox.Show("поиск завершен", "Успех",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                startbutton.Invoke((MethodInvoker)(() => startbutton.Enabled = true));
                pause.Invoke((MethodInvoker)(() => pause.Enabled = false));
                stop.Invoke((MethodInvoker)(() => stop.Enabled = false));
                directory.Invoke((MethodInvoker)(() => directory.Enabled = true));
                filemask.Invoke((MethodInvoker)(() => filemask.Enabled = true));
                filecontains.Invoke((MethodInvoker)(() => filecontains.Enabled = true));
                pause.Invoke((MethodInvoker)(() => pause.Text = "пауза"));
                mre = false;
            });            
        }
        
        private void pause_Click(object sender, EventArgs e)
        {
            if (mre==false)
            {
                manualResetEvent.Reset();
                pause.Text = "возобновить";
                mre = true;
                timer1.Stop();
                return;
            }
            else
            {                
                manualResetEvent.Set();
                pause.Text = "пауза";
                mre = false;
                timer1.Start();
                return;
            }                  
          
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            TimeSpan elapsed = sw.Elapsed;
            time.Invoke((MethodInvoker)(() => time.Text = String.Format("{0:00}:{1:00}:{2:00}",
            Math.Floor(elapsed.TotalHours), elapsed.Minutes,
            elapsed.Seconds, elapsed.Milliseconds)));
            time.Refresh();
        }
        private void stop_Click(object sender, EventArgs e)
        {
            timer1.Stop();            
            mre = false;
            pause.Text = "пауза";
            manualResetEvent.Set();
            ts.Cancel();
            System.Threading.Thread.Sleep(1000);
            ts.Dispose();
            ts = new CancellationTokenSource();
            startbutton.Invoke((MethodInvoker)(() => startbutton.Enabled = true));
            pause.Invoke((MethodInvoker)(() => pause.Enabled = false));
            stop.Invoke((MethodInvoker)(() => stop.Enabled = false));
            directory.Invoke((MethodInvoker)(() => directory.Enabled = true));
            filemask.Invoke((MethodInvoker)(() => filemask.Enabled = true));
            filecontains.Invoke((MethodInvoker)(() => filecontains.Enabled = true));
        }
        
        private void selectdirectorybutton_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                directory.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.directory = directory.Text;
            Properties.Settings.Default.filemask = filemask.Text;
            Properties.Settings.Default.filecontains = filecontains.Text;
            Properties.Settings.Default.Save();
        }
    }
}