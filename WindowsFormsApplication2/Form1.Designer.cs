﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.selectdirectorybutton = new System.Windows.Forms.Button();
            this.directory = new System.Windows.Forms.TextBox();
            this.count = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.startbutton = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.currentfiletextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.filemask = new System.Windows.Forms.TextBox();
            this.filecontains = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.pause = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // selectdirectorybutton
            // 
            this.selectdirectorybutton.Location = new System.Drawing.Point(987, 35);
            this.selectdirectorybutton.Name = "selectdirectorybutton";
            this.selectdirectorybutton.Size = new System.Drawing.Size(75, 23);
            this.selectdirectorybutton.TabIndex = 0;
            this.selectdirectorybutton.Text = "Выбрать";
            this.selectdirectorybutton.UseVisualStyleBackColor = true;
            this.selectdirectorybutton.Click += new System.EventHandler(this.selectdirectorybutton_Click);
            // 
            // directory
            // 
            this.directory.Location = new System.Drawing.Point(37, 35);
            this.directory.Name = "directory";
            this.directory.Size = new System.Drawing.Size(938, 20);
            this.directory.TabIndex = 1;
            // 
            // count
            // 
            this.count.AutoSize = true;
            this.count.Location = new System.Drawing.Point(103, 148);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(13, 13);
            this.count.TabIndex = 2;
            this.count.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Количество";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Обрабатываемый файл";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 304);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Результат";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(34, 329);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(1028, 185);
            this.treeView1.TabIndex = 7;
            // 
            // startbutton
            // 
            this.startbutton.Location = new System.Drawing.Point(78, 220);
            this.startbutton.Name = "startbutton";
            this.startbutton.Size = new System.Drawing.Size(174, 23);
            this.startbutton.TabIndex = 8;
            this.startbutton.Text = "СТАРТ";
            this.startbutton.UseVisualStyleBackColor = true;
            this.startbutton.Click += new System.EventHandler(this.startbutton_Click);
            // 
            // stop
            // 
            this.stop.Enabled = false;
            this.stop.Location = new System.Drawing.Point(719, 220);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(181, 23);
            this.stop.TabIndex = 9;
            this.stop.Text = "СТОП";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // currentfiletextbox
            // 
            this.currentfiletextbox.Location = new System.Drawing.Point(34, 194);
            this.currentfiletextbox.Name = "currentfiletextbox";
            this.currentfiletextbox.Size = new System.Drawing.Size(1030, 20);
            this.currentfiletextbox.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(259, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Прошло времени";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Location = new System.Drawing.Point(359, 148);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(49, 13);
            this.time.TabIndex = 12;
            this.time.Text = "00:00:00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Директория";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Маска файла";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Содержание";
            // 
            // filemask
            // 
            this.filemask.Location = new System.Drawing.Point(115, 61);
            this.filemask.Name = "filemask";
            this.filemask.Size = new System.Drawing.Size(255, 20);
            this.filemask.TabIndex = 16;
            // 
            // filecontains
            // 
            this.filecontains.Location = new System.Drawing.Point(115, 100);
            this.filecontains.Name = "filecontains";
            this.filecontains.Size = new System.Drawing.Size(255, 20);
            this.filecontains.TabIndex = 17;
            // 
            // pause
            // 
            this.pause.Enabled = false;
            this.pause.Location = new System.Drawing.Point(351, 220);
            this.pause.Name = "pause";
            this.pause.Size = new System.Drawing.Size(231, 23);
            this.pause.TabIndex = 18;
            this.pause.Text = "pause";
            this.pause.UseVisualStyleBackColor = true;
            this.pause.Click += new System.EventHandler(this.pause_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 526);
            this.Controls.Add(this.pause);
            this.Controls.Add(this.filecontains);
            this.Controls.Add(this.filemask);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.time);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.currentfiletextbox);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.startbutton);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.count);
            this.Controls.Add(this.directory);
            this.Controls.Add(this.selectdirectorybutton);
            this.Name = "Form1";
            this.Text = "Search";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectdirectorybutton;
        private System.Windows.Forms.TextBox directory;
        private System.Windows.Forms.Label count;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button startbutton;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.TextBox currentfiletextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox filemask;
        private System.Windows.Forms.TextBox filecontains;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button pause;
        private System.Windows.Forms.Timer timer1;
    }
}

